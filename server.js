/*jslint node: true */
'use strict';

var express = require('express'),
    cors = require('cors'),
    game = require('./routes/games');

var app = express();

app.configure(function () {
    app.use(express.logger('dev')); /* 'default', 'short', 'tiny', 'dev' */
    app.use(express.bodyParser());
    app.use(cors());
});

app.get('/games', game.index);
app.get('/games/create/:newPuzzle', game.create);
app.get('/games/:id', game.retrieve);
app.get('/games/play/:id/:newGuess', game.play);

app.listen(3000);
console.log('Listening on port 3000...');