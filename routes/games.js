/*jslint node: true */
'use strict';

var mongo = require('mongodb');

var Server = mongo.Server,
    Db = mongo.Db,
    BSON = mongo.BSONPure;

var server = new Server('localhost', 27017, {
    auto_reconnect: true
});
var db = new Db('gamedb', server);

db.open(function (err, db) {
    if (!err) {
        console.log("Connected to 'gamedb' database");
        db.collection('games', {
            strict: true
        }, function (err, collection) {
            if (err) {
                console.log("The 'games' collection doesn't exist. ");
            }
        });
    }
});

var maskPuzzle = function (game) {
    var pattern = new RegExp('[^ ' + game.guesses + ']', 'gi'),
        masked = game.puzzle.replace(pattern, '_');
    console.log(pattern);
    console.log(masked);
    game.puzzle = masked;
    return game;
};

var getGame = function (game, games, req, res, next) {
    games.findOne({
        '_id': new BSON.ObjectID(req.params.id)
    }, game, {
        safe: true
    }, function (err, game) {
        console.log("Retrieving id " + req.params.id);
        console.log(JSON.stringify(game));
        next(game, games, req, res);
    });
};

var updateMissIfNecessary = function (game, newGuess) {
    if (game.puzzle.indexOf(newGuess) < 0) {
        console.log('There have been ' + game.misses + ' already');
        if (game.misses < 6) {
            console.log('Guess not in puzzle, incrementing current ' + game.misses + ' misses');
            game.misses = game.misses + 1;
            console.log(JSON.stringify(game));
        } else {
            console.log('Show some mercy, this guy is already dead');
        }
    } else {
        console.log('Guess was good, no change to misses');
    }
};

var updateGuess = function (game, newGuess) {
    console.log('Adding guess ' + newGuess + ' to existing guesses ' + game.guesses);
    game.guesses = game.guesses + newGuess;
    console.log(game.guesses);
    console.log(JSON.stringify(game));
};

var saveTurn = function (game, req, res) {
    db.collection('games', function (err, games) {
        games.update({
            '_id': new BSON.ObjectID(req.params.id)
        }, game, {
            safe: true
        }, function (err, games) {
            if (err) {
                console.log('Error updating game: ' + err);
                res.send({
                    'error': 'An error has occurred'
                });
            } else {
                var msg = String(games) + ' document(s) updated';
                console.log(msg);
                res.send(maskPuzzle(game));
            }
        });
    });
};

exports.index = function (req, res) {
    db.collection('games', function (err, games) {
        games.find().toArray(function (err, activeGames) {
            console.log(JSON.stringify(activeGames));
            console.log(activeGames);
            activeGames.forEach(function (entry) {
                entry = maskPuzzle(entry);
            });
            res.send(activeGames);
        });
    });
};

exports.retrieve = function (req, res) {
    var game,
        returnResponse = function (game, games, req, res) {
            res.send(maskPuzzle(game));
        };

    db.collection('games', function (err, games) {
        getGame(game, games, req, res, returnResponse);
    });
};

exports.create = function (req, res) {
    var puzzle = req.params.newPuzzle.toUpperCase();
    console.log(req.params);
    db.collection('games', function (err, games) {
        console.log("Creating a new game for puzzle " + puzzle);
        games.insert({
            "misses": 0,
            "guesses": "",
            "puzzle": puzzle
        });
        res.send(true);
    });
};

exports.play = function (req, res, next) {
    var game,
        newGuess,
        puzzle,
        applyTurn = function (game, games, req, res) {
            newGuess = req.params.newGuess.toUpperCase();
            if (game.guesses.indexOf(newGuess) < 0) {

                updateMissIfNecessary(game, newGuess);

                updateGuess(game, newGuess);

                saveTurn(game, req, res);

            } else {
                console.log('Already guessed, skipping straight to response');
                res.send(maskPuzzle(game));
            }
        };

    //get the old game, then apply the turn
    db.collection('games', function (err, games) {
        getGame(game, games, req, res, applyTurn);
    });
};